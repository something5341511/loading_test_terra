import http from 'k6/http';
import {sleep} from 'k6';
import {fail} from 'k6';
import {randomIntBetween} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';

const tokens = []

export const options = {
    stages: [
        // { duration: '1m', target: 100 }, // Начните с 100 VUs в течение 1 минуты
        // { duration: '1m', target: 200 }, // Затем оставьте 200 VUs в течение 4 минуты
        // Продолжайте увеличивать и оставлять VUs, пока не достигнете нужного числа
        { duration: '5m', target: 5000 } // Достигните 1000 VUs в течение 1 минуты
    ],
    ext: {
        loadimpact: {
            // Project: Default project
            projectID: 3685433,
            // Test runs with the same name groups test runs together.
            name: 'Test (07/03/2024-11:26:18)'
        }
    }
};

export default function () {
    const token = signIn()
    if (token === undefined){
        fail("Cannot signIn")
    }

    getButtonsName(token)
    sleep(20)
    getUserInfo(token)
    sleep(20)
    getMentors(token, 33)
    sleep(20)
    getTerraEvents(token)
    sleep(20)
    signOut(token)
    sleep(20)
}

function execRequest(requestFunc, token, name) {
    const start = new Date();
    requestFunc(token);
    const end = new Date();
    console.log(`${name} Response Time: ${end - start} ms`);
}

function signOut(token){
    const url = 'https://mobile-api.terraprod.ru/auth/sign_out';

    const params = {
        headers: {
            'Authorization': token,
        },
    };

    const response = http.post(url, null, params);
    // console.log(token);
    console.log(`signOut Response Time: ${response.timings.duration} ms`);
}

function signIn() {
    const url = 'https://mobile-api.terraprod.ru/auth/sign_in';


    const payload = JSON.stringify({
        username: 'savelkayamil@yandex.ru',
        userPassword: 'qwerty'
    });

    const params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = http.post(url, payload, params);
    if (response.status !== 200) {
        return undefined;

    }
    const token = response.json().apikey;
    // console.log(token);
    console.log(`SignIn Response Time: ${response.timings.duration} ms`);
    return token

}

function getUserInfo(token) {
    const url = 'https://mobile-api.terraprod.ru/auth/get_user_info';

    const headers = {
        'Authorization': token,
        'Content-Type': 'application/json',
    };
    const response = http.post(url, null, {headers});
    console.log(`getUserInfo Response Time: ${response.timings.duration} ms; status code: ${response.status}`);

}

function getMentors(token, eventID) {

    const url = 'https://mobile-api.terraprod.ru/event/student/fetch_mentors'

    const payload = JSON.stringify({
        eventID: eventID,
    });

    const headers = {
        'Authorization': token,
        'Content-Type': 'application/json',
    }
    const response = http.post(url, payload, {headers});
    console.log(`getMentors Response Time: ${response.timings.duration} ms; status code: ${response.status}`);
}

function getTerraEvents(token) {
    const url = 'https://mobile-api.terraprod.ru/event/fetch_terra_events'

    const payload = JSON.stringify({
        filter: 0
    });

    const headers = {
        'Authorization': token,
        'Content-Type': 'application/json',
    }
    const response = http.post(url, payload, {headers});
    console.log(`getTerraEvents Response Time: ${response.timings.duration} ms; status code: ${response.status}`);
}

function getButtonsName(token){
    const url = 'https://mobile-api.terraprod.ru/event/fetch_buttons_name'


    const headers = {
        'Authorization': token,
        'Content-Type': 'application/json',
    }
    const response = http.post(url, headers, {headers});
    console.log(`getButtonsName Response Time: ${response.timings.duration} ms; status code: ${response.status}`);
}
